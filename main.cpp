#include "pico/stdlib.h" // NOLINT(modernize-deprecated-headers)

class BlinkMoFo {

private:
    uint LED_PIN; // 25 is the default LED on the pico
public:
    explicit BlinkMoFo(uint led_pin) {
        LED_PIN = led_pin;
        gpio_init(LED_PIN);
        gpio_set_dir(LED_PIN, GPIO_OUT);
    }

    void turnOn(uint time) const {
        gpio_put(LED_PIN, true);
        sleep_ms(time);
    }

    void turnOff(uint time) const {
        gpio_put(LED_PIN, false);
        sleep_ms(time);
    }

};


#pragma clang diagnostic push
#pragma ide diagnostic ignored "EndlessLoop"
int main() {

    BlinkMoFo blink = BlinkMoFo(25);

    while (true) {
        blink.turnOn(1000);
        blink.turnOff(500);
    }
}
#pragma clang diagnostic pop
